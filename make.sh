#!/bin/bash
rm *.pyc
git clone https://bitbucket.org/webhamster/pypcd8544.git
git clone https://bitbucket.org/webhamster/swt-trier-echtzeitauskunft.git
cp swt-trier-echtzeitauskunft/swtdecode.py .
cp pypcd8544/{font.py,grafix.py,pypcd8544_piandmore.py} .
mv pypcd8544_piandmore.py pypcd8544.py
rm -rf swt-trier-echtzeitauskunft
rm -rf pypcd8544

