#!/usr/bin/python
import swtdecode
from time import sleep, time
from flock import file_lock
from sys import argv, exit

LINES = 6
WIDTH = 14
CONTRAST = 16
STOPCODE = 'UNI'
DELAY = 25
DELAY_LONG = 300
DELAY_THRESHOLD_MINUTES = 13
ERROR_DELAY = 100
STATUS_CHARS = {-1: "-", 0: " ", 1: "+"}

LOCK_FILE = '/tmp/i2c.lock'

SIMULATE = '--sim' in argv

if not SIMULATE:
    try:
        from pypcd8544 import *
    except ImportError, e:
        exit("Run with --sim or provide pypcd8544.py.")

    with file_lock(LOCK_FILE):
        begin(CONTRAST, False)

def show_text(string, size=1):
    start = time()
    for i in range(0, len(string)/WIDTH):
        print string[i*WIDTH:i*WIDTH+WIDTH]
    if not SIMULATE:
        with file_lock(LOCK_FILE):
            clearDisplay()
            setCursor(0,0)
            setTextSize(size)
            setTextColor(True, False)
            write(string)
            display()
    end = time()
    print "Printed text in %f seconds." % (end - start)

while True:
    print "Requesting data for stop %s" % STOPCODE
    try:
        bl = sorted(swtdecode.get_data(STOPCODE, DELAY), key=lambda x: x.get_actual_minutes_from_now())[:LINES]
    except Exception, e:
        print e
        show_text("ERROR", 2)
        sleep(ERROR_DELAY)
        continue
        
    print "Printing text"
    next_bus_minutes = None
    out = []
    last_line = None
    for b in bl:
        if b.line == last_line:
            line = " "
        else:
            line = "%s " % b.line
        last_line = b.line
        first = ("%s%s%s" % (line, b.destination, ' ' * WIDTH))
        actual = b.get_actual_minutes_from_now()
        if actual <= 0:
            actual_text = '*'
        else:
            actual_text = '%d' % actual
        if next_bus_minutes == None:
            next_bus_minutes = actual
        planned = b.get_planned_minutes_from_now()
        sign = STATUS_CHARS[b.get_status()]
        times = "%s%s" % (sign, actual_text)
        text = "%s%s" % (first[:WIDTH-len(times)], times)
        out.append(text)
        
    show_text(''.join(out))
    if next_bus_minutes >= DELAY_THRESHOLD_MINUTES:
        print "Using long delay"
        sleep(DELAY_LONG)
    else:
        print "Using short delay"
        sleep(DELAY)

