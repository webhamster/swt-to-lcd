import os, sys, time
from contextlib import contextmanager

@contextmanager
def file_lock(lock_file):
    while os.path.exists(lock_file):
	time.sleep(0.1)
    else:
        open(lock_file, 'w').write("1")
        try:
            yield
        finally:
            os.remove(lock_file)
